# HTML/CSS/GRID
My html/css/grid


## Preview :mag:
![Image alt](https://bitbucket.org/baron1xxx/html-css-winter-2021/src/master/images/preview-readme.png)

## Links :electric_plug:

- [Zeit Now](https://html-css-winter-2021.vercel.app/)



## :boom: Created by *Roman Mykytka 2021* :rocket:
